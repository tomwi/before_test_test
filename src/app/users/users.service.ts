import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class UsersService {
  usersObservable;

  constructor(private af:AngularFire) { }
  
deleteUser(user){
    this.usersObservable.remove(user);
}

  addUser(user){
    this.usersObservable.push(user);
  }
 editUser(user){
let usertemp = {name:user.name,email:user.email}
console.log(usertemp);
this.af.database.object('/users/' + user.$key).update(usertemp);
 }

  setUsers(){
    this.usersObservable = this.af.database.list('/users').map(
      users =>{
        users.map(
          user => {
            user.posTitles = [];
            for(var p in user.posts){
                user.posTitles.push(
                this.af.database.object('/posts/' + p)
              )
            }
          }
        );
        return users;
      }
    )
    //this.usersObservable = this.af.database.list('/users');
    return this.usersObservable;
	}
}