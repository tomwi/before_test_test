import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class UsersComponent implements OnInit {

 users;

 currentUser;

  ngOnInit(){

    this._usersService.setUsers().subscribe(userData => {this.users = userData;console.log(this.users)});
  }

  constructor(private _usersService: UsersService) {
    //this.users = this._userService.getUsers();
  }
  addUser(user){
    this._usersService.addUser(user);
  }
  deleteUser(user){
    this._usersService.deleteUser(user);
  }
editUser(user){
this._usersService.editUser(user)
}
}
