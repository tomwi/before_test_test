import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Product} from './product';



@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
    inputs:['product']

})
export class ProductComponent implements OnInit {
product:Product;
isEdit:boolean = false;
editButtonText = 'Edit';


@Output() deleteEvent = new EventEmitter<Product>();
@Output() updateEvent = new EventEmitter<Product>();

updateProduct(){
this.isEdit = !this.isEdit;
this.editButtonText ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
if(!this.isEdit){
 this.updateEvent.emit(this.product);
  }
}

sendDelete(){
  this.deleteEvent.emit(this.product);
 
}
  constructor() { 
         
  }

  ngOnInit() {

  }

}
